# Утилита для выбора и перепаковки книг нужных жанров из полного архива флибусты
# для работы утилиты надо создать каталоги ''out'.
# исходные файлы поместить в каталог где находится скрипт
# обработанные архивы будут сохраняться в 'out'.
# индексы архивов должны быть в каталоге 'index', а
# описание жанров должны быть распложены в одной папке со скриптом
import os
import collections
import zipfile
Book = collections.namedtuple('Book', 'genre file size lang')


def parse_genres():
    genres = []
    with open("genres.org", encoding="utf8") as f:
        for line in f:
            if line.startswith('* '):
                e = line[2:-1].split(';')
                genres.append(e[1])
    return set(genres)


def parse_file(filename, genres):
    with open(filename, encoding="utf8") as f:
        for line in f:
            items = line.split('\x04')
            book = Book(items[1][:-1], items[5], int(items[6]), items[11])
            if book.lang == 'ru' and book.genre in genres:
                yield book


def process_all():
    genres = parse_genres()
    for arch_desc in (file for file in os.listdir('index') if file.endswith('inp')):
        arch_name = "{}.zip".format(arch_desc[:-4])
        in_arch_name = arch_name
        if os.path.exists(in_arch_name):
            print(in_arch_name)
            out_arch_name = os.path.join('out', arch_name)
            with zipfile.ZipFile(in_arch_name) as zi,\
                 zipfile.ZipFile(out_arch_name, mode='w') as zo:
                for b in parse_file(os.path.join('index', arch_desc), genres):
                    bookname = b.file + '.fb2'
                    try:
#                          print(bookname)
                       zi.extract(bookname, path='out')
                       os.chdir('out')
                       zo.write(bookname)
                       os.unlink(bookname)
                       os.chdir('..')
                    except KeyError:
                        pass

process_all()
